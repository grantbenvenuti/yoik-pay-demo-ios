//
//  PNIViewController.m
//  PNI YOIK Test
//
//  Created by Grant Benvenuti on 24/02/2014.
//  Copyright (c) 2014 Payment Network International Pty Ltd. All rights reserved.
//

#import "PNIViewController.h"

@interface PNIViewController ()

@end

@implementation PNIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
            NSLog(@"here i am");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)payButton:(id)sender {
    
    //load url into webview
    NSString *baseURL = @"https://pay.yoik.net/form";
    NSString *merchant = @"test0";
    NSString *userId = @"userx";
    NSString *bearer = @"aewuWKCsXcVPhD4eEFFBxBDrXr";
    NSString *redirect = @"pniyoik://go"; // our custom domain as setup in the plist file.
    
    NSString *encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                                    NULL,
                                                                                                    (CFStringRef)redirect,
                                                                                                    NULL,
                                                                                                    (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                                    kCFStringEncodingUTF8 ));
    
    NSString *strURL = [NSString stringWithFormat:@"%@?merchantid=%@&userid=%@&bearer=%@&redirecturl=%@", baseURL, merchant, userId, bearer, encodedString];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: strURL]];
    
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://pay.yoik.net/form?merchantid=test0&userid=userx&bearer=aewuWKCsXcVPhD4eEFFBxBDrXr&redirecturl=pniyoik%3A%2F%2Fgo%3A9020"]];
}

@end
