//
//  PNIAppDelegate.h
//  PNI YOIK Test
//
//  Created by Grant Benvenuti on 24/02/2014.
//  Copyright (c) 2014 Payment Network International Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PNIAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
